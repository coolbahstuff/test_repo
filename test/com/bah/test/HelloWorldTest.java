/**
 * 
 */
package com.bah.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author andrewfreeman
 *
 */
public class HelloWorldTest {

	private HelloWorld test;
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		test = new HelloWorld();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		assert(true);
		assertFalse(test.getVar1()==2);
	}
	
	@Test
	public void test2() {
		assert(true);
		assertTrue(test.getVar1()==1);
	}

}
